﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW9_Multithread
{
    class LinqSumm : IArrayCalc
    {
     public int GetSum(int[] array)
        {
            return array.AsParallel().Sum();
        }
    }
}
